# ASR Cleaning

## Steps
1. Upload the video to [ASR transcription webpage](https://asr.iitm.ac.in/NPTEL/Transcribe/) (or [here](https://asr.iitm.ac.in/web_demo/asr_SRT/)) to get the ASR transcription in SRT format. (Name format: ``Week<week-num>_Lecture<lecture-num>_ASR_IITM.srt``)
2. Upload (git push) the SRT file to the git repo location: ``nltm-iitm-repo/NPTEL_Lecture_Cleaning/<lecture-folder>/ASR_Cleaning``
> Upload before correction of SRTs and also after correction.
3. Correct the SRTs. Refer next section (inside ``ASR_Cleaning`` folder) for guidelines on SRT cleaning.
4. Tool to correct the SRTs: [Subtitle Edit](https://www.nikse.dk/subtitleedit/).
- This software can be installed in both Linux and Windows systems.
- Windows installation file download: [SubtitleEdit-3.6.5-Setup.zip](https://github.com/SubtitleEdit/subtitleedit/releases/download/3.6.5/SubtitleEdit-3.6.5-Setup.zip)
- Linux installation file download: [SE365.zip](https://github.com/SubtitleEdit/subtitleedit/releases/download/3.6.5/SE365.zip)
- Linux installation [readme](https://nikse.dk/SubtitleEdit/Help#linux) (requirements are given here.)
- It is not mandatory to use this tool itself, but this tool will help you have play video, edit srt text and correct alignment of SRT with audio in the same window.
5. Upload (git push) the SRT file after correction to the same git repo location. (Name format: ``Week<week-num>_Lecture<lecture-num>_Cleaned.srt``. Example: ``Week1_Lecture1_Cleaned.srt``)
> If verbatim SRT correction is done, then please upload (git push) the file in the name format ``Week<week-num>_Lecture<lecture-num>_Verbatim.srt``

## Guidelines for SRT Cleaning
- Two version of SRTs and Glossary file have to be created for ASR cleaning. Each part of ASR cleaning is explained in detail below. Please read and make sure you follow it diligently.
1. **Verbatim SRT**: To create clean lecture dataset for ASR training and evaluation. Name Format: ``Week<week-num>_Lecture<lecture-num>_Verbatim.srt``
2. **Corrected SRT**: To make sure the *Machine Translation(MT)* works well without loss of information and also in a way that the lecturer meant to convey the message. Name Format: ``Week<week-num>_Lecture<lecture-num>_Cleaned.srt``
3. **Glossary file**: List of technical words in each lecture that shouldn't be translated to the target language, but instead  be kept as is. Location: ``nltm-iitm-repo/NPTEL_Lecture_Cleaning/<lecture-folder>/Technical_Words_Glossary/Week<week-num>_Lecture<lecture-num>_Glossary.txt``
4. **Tagging technical words**: List of technical words in each lecture that shouldn't be translated to the target language, but instead should be kept as is.
5. **Comments file**: Give comments on confusions and issues faced during ASR cleaning up.

### 1. Verbatim SRT
> **Aim**: Having gold standard labeling for lecture videos for ASR evaluations and ASR training.
- *To Do*
1. Make sure that the SRT is in line with the verbatim spoken by the lecturer. (i.e,) Correct the ASR errors like substitutions, insertions and deletions.
2. Make alignments correct with the SRT text, But **don't** merge or split the srt segments. (i.e,) Number of srt segments will be same between ``Week<week-num>_Lecture<lecture-num>_ASR_IITM.srt`` and ``Week<week-num>_Lecture<lecture-num>_Verbatim.srt``.
3. **Disfluencies**: Keep the disfluencies as it is. **Don't remove** filler words like "ahh", "hmm", "uhh" etc.
- Also add these filler words in case if they are missing with tags like, ``<FW>``filler-word``</FW>``. Words like "okay", "right", "so" (included in dictionary) need not have these tags instead only for the words like "ahh", "hmm", "uhh".
- In case of additional noises such as "cough", "keyboard clicks" etc. please add them as ``<NS>``cough``</NS>`` and ``<NS>``keyclicks``</NS>``.
- If the word spoken is not clear, add ``<NC>``?``</NC>``
4. **Numbers**: Keep numbers as it is. But, if the transcription already has a number spelled out, do not change it. For example, do not change "python 3.7" to "python three point seven". Just keep it the same. Similarly, do not change "there are two versions" to "there are 2 versions".
5. Correct **punctuations** and **cases(lowercase/uppercase)** wherever possible.
- *Don'ts*
1. Don't split or merge SRTs.
2. Don't add technical word tags (explained below)

> Upload to ``nltm-iitm-repo/NPTEL_Lecture_Cleaning/<lecture-folder>/ASR_Cleaning/Week<week-num>_Lecture<lecture-num>_Verbatim.txt``

### 2. Corrected SRT
> **Aim**: To get the best Machine Translation (MT) for the lecture to target language without losing the meaning of what the lecturer is conveying.
> Copy ``Verbatim SRT`` and do the additional changes for ``Corrected SRT``.
- *To Do*
1. *Merge or split segments* as and when necessary to make *meaning of sentences correct* without loss of context for the MT to have proper translations to target languages.
2. *Change the word order* if required to make the sentences meaningful.
3. Spell "point" instead of "." and similarly for the comma, *in the case if it has to be spelled out (spoken) in the lecture*. **Not** in all the end of sentences.
4. Again as in ``Verbatim SRT`` the alignments of SRT text with audio has to be corrected in ``Corrected SRT``. This will really improve LipSyncing quality.
5. **Tag technical terms** and parts of sentences that has to kept as it is in translated text. How to give tags are explained below in detail.
6. **Glossary file**: Add technical words to the glossary file. Details of how to do it is given below.

> Upload to ``nltm-iitm-repo/NPTEL_Lecture_Cleaning/<lecture-folder>/ASR_Cleaning/Week<week-num>_Lecture<lecture-num>_Cleaned.txt``

### 3. Glossary file
> **Aim**: To make sure that technical terms when converted to the target language does not  lose its meaning.
- *To Do*
1. Add technical words that should not  get confused with other general words to the glossary file. (i.e,) The glossary term should be kept as is (in source language) in all the places throughout the lecture. Only then, add that word to glossary file. Use tags (explained below) for the ones that have to kept as it is only in a few cases and make sure **not** to add that word in glossary file.
- *Don'ts*
1. Don't have the same word with tag and in glossary file. This can be confusing.

> Upload to ``nltm-iitm-repo/NPTEL_Lecture_Cleaning/<lecture-folder>/Technical_Words_Glossary/Week<week-num>_Lecture<lecture-num>_Glossary.txt``

### 4. Tagging technical words
> **Aim**: For some words, depending on the context we may have to translate or transliterate. For such cases, we will use tags to state that these words/phrases should be transliterated.
- For example, consider the following sentence, "There are two versions of python. One is python two point seven."Here, the underlined words, we want to translate. But, the bold italicized words, we want to transliterate. So, we will change the sentence to this: "There are two versions of python. One is python ``<DT>``two``</DT>`` point ``<DT>``seven``</DT>``."
- *To Do*
1. For **Domain terms** we need annotate text with ``<DT>``, ``</DT>`` tag. Example: Lets us learn ``<DT>``database programing``</DT>`` today.
2. For text, which we need to keep as it is in **Roman**, we need to add ``<DNT>``, ``</DNT>`` tag. Example: let us check following example ``<DNT>``Ram is at school``</DNT>``.

### 5. Comments file
> Please give your comments on ASR cleaning in the comments.
> Like, issues with alignment. If there is any issues with a specific word going wrong or anything like that.
> Also give the statistics of how much time it has taken for each stage in this effort.
- *To Do*
1. Upload the text file to ``nltm-iitm-repo/NPTEL_Lecture_Cleaning/<lecture-folder>/ASR_Cleaning/Week<week-num>_Lecture<lecture-num>_Comments.txt``.
